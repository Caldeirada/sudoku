import React from 'react';
import game_board_styles from './gameTile.module.css';

export interface GameTileProps {
    white: boolean;
    num: number;
    onMouseOver: () => void;
}
{/* <div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div> */}

const GameTile: React.FC<GameTileProps> = ({ white, num, onMouseOver }) => {
    return (
        <div className={`${game_board_styles.box} ${white ? game_board_styles.white_background : game_board_styles.gray_background}`} onMouseOver={onMouseOver}>
            {num === 0 ? undefined : num}
        </div>
    );
};


export { GameTile }