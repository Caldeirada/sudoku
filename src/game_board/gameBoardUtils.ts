interface Row {
    numbers: number[]
}

interface Board {
    rows: Row[];
}

const NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const ZEROS = [0, 0, 0];

const getArray = () => Array.from(Array(10).keys()).splice(1, 10).sort(() => Math.random() - 0.5);


const getRow = (): Row => {
    return {
        numbers: getArray()
    };
}

const genSquares = (numbers: number[]) => {
    return [numbers.slice(0, 3), numbers.slice(3, 6), numbers.slice(6, 9)]
}

const genGameboard = (): Board => {
    const row1 = genSquares(getArray()).map(row => row.concat(ZEROS).concat(ZEROS))
    const row2 = genSquares(getArray()).map(row => ZEROS.concat(row).concat(ZEROS))
    const row3 = genSquares(getArray()).map(row => ZEROS.concat(ZEROS).concat(row))
    const numbers = [...row1, ...row2, ...row3];


    return {
        rows: numbers.map(row => { return { numbers: row } })
    }
}

const getGameboard = (): Board => {
    var gameBoard = genGameboard()

    return gameBoard;
};


export { getGameboard }