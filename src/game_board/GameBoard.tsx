import React, { useState } from 'react';
import { GameTile } from '../game_tile/Gametile';
import game_board_styles from './gameBoard.module.css';
import { getGameboard } from './gameBoardUtils';

export interface GameBoardProps {
    num: number;
    correct?: boolean;
}
{/* <div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div>
<div className={game_board_styles.box}>200px x 200px</div> */}

export const GameBoard: React.FC = () => {
    const [gameBoard, setGameBoard] = useState(getGameboard());

    var white = true;
    return <div className={game_board_styles.game_board}>
        {gameBoard.rows.map((row, i) => {
            if (i % 3 === 0) {
                white = !white
            }
            white = !white
            return row.numbers.map((element, j) => {
                if (j % 3 === 0) {
                    white = !white
                }
                return <GameTile white={white} num={element} onMouseOver={() => console.log(`i:${i} j:${j}`)} />
            })
        })}
    </div>
};